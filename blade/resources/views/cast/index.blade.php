@extends('layouts.master')


@section('konten')
    
<a href="/cast/create" class="btn btn-success btn-sm my-2">Tambah</a>

<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item) <!--Looping data-->
            <tr>
                <td> {{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->umur}}</td>
                <td>{{$item->bio}}</td>
                <td>
                    <a href="/cast/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                </td>
            </tr>
        @empty
            <tr>
                <td>Tidak ada Data</td>
            </tr>
        @endforelse
    </tbody>
  </table>

@endsection


<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        $validatedData = $request->validate([
            'nama' => 'required|unique:cast,nama|max:255',
            'umur' => 'required|integer',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'Inputan nama tidak boleh kosong',
            'umur.required' => 'Inputan umur tidak boleh kosong',
            'bio.required' => 'Inputan bio tidak boleh kosong',
        ]);    

        DB::table('cast')->insert(
            [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio'],
            ]
        );

        return redirect('/cast'); //berfungsi sebagai pengembali ke halaman create setelah mengisi form
    }

    public function index()
    {
        $cast = DB::table('cast')->get();

        return view('cast.index', ['cast' => $cast]);
    }

    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('cast'));
    }
   
    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));

    }
        public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|unique:cast,nama|max:255',
            'umur' => 'required|integer',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'Inputan nama tidak boleh kosong',
            'umur.required' => 'Inputan umur tidak boleh kosong',
            'bio.required' => 'Inputan bio tidak boleh kosong', 
        ]
        );

        $query = DB::table('post')
            ->where('id', $id)
            ->update([
                'nama' => $request["nama"],
                'umur' => $request["umur"],
                'bio' => $request["bio"],
            ]);
        return redirect('/cast');
    }
}
